'use strict'

const path = require('path')
const Package = require('@kpzc-cli-dev/package')
const log = require('@kpzc-cli-dev/log')
const { exec: spawn } = require('@kpzc-cli-dev/utils')
const SETTINGS = {
  init: '@kpzc-cli-dev/init',
  publish: '@kpzc-cli-dev/publish',
  add: '@kpzc-cli-dev/add'
}
const CACHE_DIR = 'dependencies'

async function exec() {
  let targetPath = process.env.CLI_TARGET_PATH
  const homePath = process.env.CLI_HOME_PATH
  log.verbose('targetPath', targetPath)
  log.verbose('homePath', homePath)
  let storeDir = ''
  const cmdObj = arguments[arguments.length - 1]
  const cmdName = cmdObj.name()
  const packageName = SETTINGS[cmdName]
  const packageVersion = 'latest'
  let pkg = ''
  // console.log(cmdName,cmdObj._optionValues.force)
  if (!targetPath) {
    targetPath = path.resolve(homePath, CACHE_DIR) //生成缓存路径
    storeDir = path.resolve(targetPath, 'node_modules')
    log.verbose('targetPath', targetPath)
    log.verbose('storeDir', storeDir)
    pkg = new Package({
      targetPath,
      storeDir,
      packageName,
      packageVersion
    })
    if (await pkg.exists()) {
      //更新package
      await pkg.update()
    } else {
      //安装package
      await pkg.install()
    }
  } else {
    pkg = new Package({
      targetPath,
      packageName,
      packageVersion
    })
  }
  const rootFile = pkg.getRootFilePath()
  log.verbose('rootFile', rootFile)
  if (rootFile) {
    try {
      // 当前进程中调用init方法
      //require(rootFile).call(null,Array.from(arguments))
      // 在node子进程中调用init方法
      const args = Array.from(arguments)
      const cmd = args[args.length - 1]
      const obj = Object.create(null)
      obj['force'] = cmd._optionValues.force
      Object.keys(cmd).forEach((key) => {
        if (
          cmd.hasOwnProperty(key) &&
          !key.startsWith('_') &&
          key !== 'parent'
        ) {
          obj[key] = cmd[key]
        }
      })
      args[args.length - 1] = obj
      const code = `require('${rootFile}').call(null,${JSON.stringify(args)})`
      const child = spawn('node', ['-e', code], {
        cwd: process.cwd(),
        stdio: 'inherit'
      })
      child.on('error', (e) => {
        log.error(e.message)
        process.exit(1)
      })
      child.on('exit', (e) => {
        log.verbose('执行命令成功:' + e)
        process.exit(e)
      })
    } catch (e) {
      log.error(e.message)
    }
  }
}
module.exports = exec
