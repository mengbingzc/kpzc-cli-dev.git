# 前端统一研发脚手架

## About

前端架构师专属脚手架

## Getting Started

### 安装：

```bash
npm install -g @kpzc-cli-dev/core
```

### 创建项目

项目/组件初始化

```bash
kpzc-cli init
```

强制清空当前文件夹

```bash
kpzc-cli init --force
```

### 发布项目

发布项目/组件

```bash
kpzc-cli publish
```

强制更新所有缓存

```bash
kpzc-cli publish --force
```

正式发布

```bash
kpzc-cli publish --prod
```

手动指定 build 命令

```bash
kpzc-cli publish --buildCmd "npm run build:test"
```

## More

清空本地缓存：

```bash
kpzc-cli clean
```

DEBUG 模式：

```bash
kpzc-cli --debug
```

调试本地包：

```bash
kpzc-cli init --packagePath /Users/xxx/Desktop/kpzc-cli-test/packages/init/
```
