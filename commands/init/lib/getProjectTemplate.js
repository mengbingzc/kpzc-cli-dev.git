const request = require('@kpzc-cli-dev/request')

module.exports = function () {
  return request({
    url: '/project/template'
  })
}
