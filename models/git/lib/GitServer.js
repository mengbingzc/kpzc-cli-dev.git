function error(methodName) {
  throw new Error(`${methodName} must be implemented!`)
}
class GitServer {
  constructor(type, token) {
    this.type = type
    this.token = token
  }
  setToken(token) {
    this.token = token
  }
  /**
   * getRepo
   * @param {*} login 用户登录名称
   * @param {*} name 项目名称
   */
  getRepo(login, name) {
    error('getRepo')
  }
  createRepo(name) {
    error('createRepo')
  }
  /**
   * createOrgRepo
   * @param {*} name 项目名称
   * @param {*} login 用户名
   */
  createOrgRepo(name, login) {
    error('createOrgRepo')
  }
  getRemote() {
    error('getRemote')
  }
  getUser() {
    error('getUser')
  }
  getOrg() {
    error('getOrg')
  }
  getTokenUrl() {
    error('getTokenUrl')
  }
  getTokenHelpUrl() {
    error('getTokenHelpUrl')
  }
}

module.exports = GitServer
