const GitServer = require('./GitServer')
const GitHubRequest = require('./GitHubRequest')
class Github extends GitServer {
  constructor() {
    super('github')
    this.request = null
  }
  setToken(token) {
    super.setToken(token)
    this.request = new GitHubRequest(token)
  }
  getUser() {
    return this.request.get('/user')
  }
  getOrg(username) {
    return this.request.get(`/user/orgs`, {
      page: 1,
      per_page: 100
    })
  }
  getRepo(login, name) {
    return new Promise((resolve, reject) => {
      this.request
        .get(`/repos/${login}/${name}`)
        .then((response) => {
          if (response.status === 404) {
            resolve(null)
          } else {
            resolve(response)
          }
        })
        .catch((err) => {
          reject(err)
        })
    })
  }
  createRepo(name) {
    return this.request.post(
      '/user/repos',
      {
        name
      },
      {
        Accept: 'application/vnd.github+json'
      }
    )
  }
  createOrgRepo(name, login) {
    return this.request.post(
      `/orgs/${login}/repos`,
      {
        name
      },
      {
        Accept: 'application/vnd.github+json'
      }
    )
  }
  getTokenUrl() {
    return 'https://github.com/settings/tokens'
  }
  getTokenHelpUrl() {
    return 'https://docs.github.com/en/github/authenticating-to-github/connecting-to-github-with-ssh'
  }
  getRemote(login, name) {
    return `git@github.com:${login}/${name}.git`
  }
}

module.exports = Github
