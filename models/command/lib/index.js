'use strict'

const log = require('@kpzc-cli-dev/log')
const semver = require('semver')
const colors = require('colors/safe')
const LOWEST_NODE_VERSION = '14.0.0'
class Command {
  constructor(argv) {
    // console.log('command constructor', argv)
    if (!argv) {
      throw new Error('参数不能为空！')
    }
    if (!Array.isArray(argv)) {
      throw new Error('参数必须是数组！')
    }
    if (argv.length < 1) {
      throw new Error('参数列表为空！')
    }
    this._argv = argv
    let runner = new Promise((resolve, reject) => {
      let chain = Promise.resolve()
      chain = chain.then(() => this.checkNodeVersion())
      chain = chain.then(() => this.initArgs())
      chain = chain.then(() => this.init())
      chain = chain.then(() => this.exec())
      chain.catch((err) => {
        log.error(err.message)
      })
    })
  }
  //初始化参数
  initArgs() {
    this._cmd = this._argv[this._argv.length - 1]
    this._argv = this._argv.slice(0, this._argv.length - 1)
  }
  //检查node版本
  checkNodeVersion() {
    //1.获取当前版本号
    const currentVersion = process.version
    //2.比对最低版本号
    const lowestVersion = LOWEST_NODE_VERSION
    if (!semver.gte(currentVersion, lowestVersion)) {
      throw new Error(
        colors.red(`kpzc-cli 需要安装 v${lowestVersion}以上版本的Node.js`)
      )
    }
  }
  //命令准备
  init() {
    throw new Error('init必须实现')
  }
  //命令执行
  exec() {
    throw new Error('exec必须实现')
  }
}

module.exports = Command
